package com.thedoctor1103.afkplus;

import java.io.File;
import java.io.InputStream;
import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * AFK But Better
 *
 * @authors TheDoctor1103 and Dart2112
 */

public class AFKPlus 
	extends JavaPlugin
{
	public final Logger logger = this.getLogger();
	private static FileConfiguration Config;
	private File configurationFile;
	
	public void onDisable()
	{
	    Bukkit.getConsoleSender().sendMessage(ChatColor.AQUA + "[AFK+] has been disabled!" + ChatColor.RESET);
	}
	
	public void onEnable()
	{
	    logTitle();
		loadConfig();
	    saveConfig();
	}
	public void logTitle()
	{
		Bukkit.getConsoleSender().sendMessage(ChatColor.WHITE + "----------------------------------------");
		Bukkit.getConsoleSender().sendMessage(ChatColor.GOLD + "[AFK+] v" + getDescription().getVersion() + " has been enabled!");
		Bukkit.getConsoleSender().sendMessage(ChatColor.WHITE + "----------------------------------------" + ChatColor.RESET);
	}
	@SuppressWarnings("deprecation")
	public void loadConfig() 
	  {
		    if (this.configurationFile == null) 
		    {
		      this.configurationFile = new File(getDataFolder(), "config.yml");
		    }
		    Config = YamlConfiguration.loadConfiguration(this.configurationFile);
		    InputStream defConfigStream = getResource("config.yml");
		    if (defConfigStream != null) {
		      YamlConfiguration defConfig = YamlConfiguration.loadConfiguration(defConfigStream);
		      Config.setDefaults(defConfig);
		    }
		  }

}
